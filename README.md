# Runway robot

## Background
 
 A robotics company named Trax has created a line of small self-driving robots 
 designed to autonomously traverse desert environments in search of undiscovered
 water deposits.

 A Traxbot looks like a small tank. Each one is about half a meter long and drives
 on two continuous metal tracks. In order to maneuver itself, a Traxbot can do one
 of two things: it can drive in a straight line or it can turn. So to make a 
 right turn, A Traxbot will drive forward, stop, turn 90 degrees, then continue
 driving straight.

 This series of questions involves the recovery of a rogue Traxbot. This bot has 
 gotten lost somewhere in the desert and is now stuck driving in an almost-circle: it has
 been repeatedly driving forward by some step size, stopping, turning a certain 
 amount, and repeating this process... Luckily, the Traxbot is still sending all
 of its sensor data back to headquarters.

 In this project, we will start with a simple version of this problem and 
 gradually add complexity. By the end, you will have a fully articulated
 plan for recovering the lost Traxbot.
 
 ----------
## Part One - studentMain1.py

 Let's start by thinking about circular motion (well, really it's polygon motion
 that is close to circular motion). Assume that Traxbot lives on 
 an (x, y) coordinate plane and (for now) is sending you PERFECTLY ACCURATE sensor 
 measurements. 

 With a few measurements you should be able to figure out the step size and the 
 turning angle that Traxbot is moving with.
 With these two pieces of information, you should be able to 
 write a function that can predict Traxbot's next location.
 
 ----------
## Part Two - studentMain2.py

 Now we'll make the scenario a bit more realistic. Now Traxbot's
 sensor measurements are a bit noisy (though its motions are still
 completetly noise-free and it still moves in an almost-circle).
 You'll have to write a function that takes as input the next
 noisy (x, y) sensor measurement and outputs the best guess 
 for the robot's next position.

 ----------
## Part Three - studentMain3.py

 Now you'll actually track down and recover the runaway Traxbot. 
 In this step, your speed will be about twice as fast the runaway bot,
 which means that your bot's distance parameter will be about twice that
 of the runaway. You can move less than this parameter if you'd 
 like to slow down your bot near the end of the chase. 
 
 ----------
## Part Four - studentMain4.py

 Again, you'll track down and recover the runaway Traxbot. 
 But this time, your speed will be about the same as the runaway bot. 
 This may require more careful planning than you used last time.
 
 ----------

## Implementation

 The problem was solved using Kalman Filter. 
 
  - To run the code for different test cases, run "testing_suite_full.py"