# ----------
# Part Four
#
# Again, you'll track down and recover the runaway Traxbot. 
# But this time, your speed will be about the same as the runaway bot. 
# This may require more careful planning than you used last time.
#
# ----------
# YOUR JOB
#
# Complete the next_move function, similar to how you did last time. 
#
# ----------
# GRADING
# 
# Same as part 3. Again, try to catch the target in as few steps as possible.


### References: This code has been taken from my previous submission to this course in Spring 2018

from robot import *
from math import *
from matrix import *
import random

def next_move(hunter_position, hunter_heading, target_measurement, max_distance, OTHER = None):
    # This function will be called after each time the target moves. 

    # The OTHER variable is a place for you to store any historical information about
    # the progress of the hunt (or maybe some localization information). Your return format
    # must be as follows in order to be graded properly.
    if OTHER is None:
        OTHER = []
        heading = 0
        turning = 0
        distance1 = 0
        xy_estimate = (target_measurement[0], target_measurement[1])
        #x = matrix([[measurement[0]], [measurement[1]], [heading], [turn], [distance1]])
        x = matrix([[target_measurement[0]], [target_measurement[1]], [heading], [turning], [distance1]])
        P = matrix([[1000,0,0,0,0],[0,1000,0,0,0],[0,0,1000,0,0],[0,0,0,1000,0],[0,0,0,0,1000]])
        OTHER.append((x,P))
        return turning, distance1, OTHER
    elif len(OTHER) == 1:
        P = matrix([[1000,0,0,0,0],[0,1000,0,0,0],[0,0,1000,0,0],[0,0,0,1000,0],[0,0,0,0,1000]])
        x = matrix([[target_measurement[0]], [target_measurement[1]], [0], [0], [0]])
        target = target_measurement
        desired_direction = target_direction(hunter_position, target)
        turning = desired_direction - hunter_heading
        distance = max_distance
        OTHER.append((x, P))
        return turning, distance, OTHER
    elif len(OTHER) == 2:
        P = matrix([[1000,0,0,0,0],[0,1000,0,0,0],[0,0,1000,0,0],[0,0,0,1000,0],[0,0,0,0,1000]])
        x = matrix([[target_measurement[0]], [target_measurement[1]], [0], [0], [0]])
        OTHER.append((x, P))
        x_dash = OTHER[-1][0]
        x_new = 2 * OTHER[-1][0].value[0][0] - OTHER[-2][0].value[0][0]
        y_new = 2 * OTHER[-1][0].value[1][0] - OTHER[-2][0].value[1][0]
        target = (x_new, y_new)
        desired_direction = target_direction(hunter_position, target)
        turning = desired_direction - hunter_heading
        distance = max_distance
        return turning, distance, OTHER
    elif len(OTHER) == 3:
        P = matrix([[1000,0,0,0,0],[0,1000,0,0,0],[0,0,1000,0,0],[0,0,0,1000,0],[0,0,0,0,1000]])
        i = len(OTHER)
        current_measurement = target_measurement
        prev_measurement = (OTHER[-1][0].value[0][0],OTHER[-1][0].value[1][0])
        prev_measurement2 = (OTHER[-2][0].value[0][0],OTHER[-2][0].value[1][0])
        distance1 = distance_between(prev_measurement,current_measurement)
        heading = atan2(current_measurement[1] - prev_measurement[1], current_measurement[0] - prev_measurement[0])
        heading_old = atan2(prev_measurement[1] - prev_measurement2[1], prev_measurement[0] - prev_measurement2[0])
        turn = heading - heading_old
        est_heading = angle_trunc(turn + heading)
        est_distance = distance1
        est_x = current_measurement[0] + est_distance * cos(est_heading)
        est_y = current_measurement[1] + est_distance * sin(est_heading)
        #measurement_vector.append(measurement)
        angle = est_heading
        x = matrix([[target_measurement[0]], [target_measurement[1]], [est_heading], [turn], [est_distance]])
        OTHER.append((x, P))
        target = (est_x, est_y)
        desired_direction = target_direction(hunter_position, target)
        turning = desired_direction - hunter_heading
        distance = max_distance
        
        return turning, distance, OTHER
    else:
#        if len(OTHER) == 4:
#            x = matrix([[OTHER[-1][0][0]], [OTHER[-1][0][1]], [OTHER[-1][0][2]], [OTHER[-1][0][3]], [OTHER[-1][0][4]]])

        i = len(OTHER)-1
        x = OTHER[-1][0]
        P = OTHER[i][1]
        heading = x.value[2]
        turn = x.value[3]
        distance1 = x.value[4]
        measurement_noise = 1
        H = matrix([[1,0,0,0,0],[0,1,0,0,0]])
        R = matrix([[measurement_noise,0],[0,measurement_noise]])
        I = matrix([[1,0,0,0,0],[0,1,0,0,0],[0,0,1,0,0],[0,0,0,1,0],[0,0,0,0,1]]) 
        u = matrix([[0.], [0.], [0.], [0.], [0.]])
        # measurement update
        Z = matrix([[target_measurement[0]],[target_measurement[1]]])
        y = Z - (H * x)
        S = H * P * H.transpose() + R
        K = P * H.transpose() * S.inverse()
        x = x + (K * y)
        #print x
        P = (I - (K * H)) * P
           
        # prediction
        distance1 = x.value[4][0]
        heading = x.value[2][0]
        turn = x.value[3][0]
        #x_dash = matrix([[x.value[0][0] + distance1 * cos(heading+turn)],[x.value[1][0] + distance1 * sin(heading+turn)],[heading+turn],[turn],[distance1]])
        x_dash = matrix([[x.value[0][0] + distance1 * cos(heading+turn)],[x.value[1][0] + distance1 * sin(heading+turn)],[heading+turn],[turn],[distance1]])
        
        dx_h = -distance1 * sin(heading + turn) 
        dx_t = -distance1 * sin(heading + turn)
        dx_d = cos(heading + turn)
        dy_h = distance1 * cos(heading + turn)
        dy_t = distance1 * cos(heading + turn)
        dy_d = sin(heading + turn) 
        F = matrix([[1,0,dx_h,dx_t,dx_d],[0,1,dy_h,dy_t,dy_d],[0,0,1,1,0],[0,0,0,1,0],[0,0,0,0,1]])
        
        P = F * P * F.transpose()
        OTHER.append((x_dash, P))
        xy_estimate = (x_dash.value[0][0],x_dash.value[1][0])
        target = xy_estimate
        difference = distance_between(target, hunter_position)
        step = 1
        while difference > step * max_distance and step < 50:
            angle = x_dash.value[2][0]
            distance = x_dash.value[4][0]
            (old_x,old_y) = target
            
            new_x = old_x + distance * cos(angle)
            new_y = old_y + distance * sin(angle)
            target = new_x, new_y
            difference = distance_between(target, hunter_position)
            step = step + 1
        
        direction = get_heading(hunter_position,target)
        turning = direction - hunter_heading
        if difference < max_distance:
            distance = difference
        else:
            distance = max_distance
            
    #print("Estimate of Iteration " +str(len(OTHER)) + "estimated value = " + str(xy_estimate))
    
    
    return turning, distance, OTHER

def target_direction(hunter_position, target_position):
    """Returns the angle, in radians, between the target and hunter positions"""
    hunter_x, hunter_y = hunter_position
    target_x, target_y = target_position
    heading = atan2(target_y - hunter_y, target_x - hunter_x)
    heading = angle_trunc(heading)
    return heading

def distance_between(point1, point2):
    """Computes distance between point1 and point2. Points are (x, y) pairs."""
    x1, y1 = point1
    x2, y2 = point2
    return sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)

def demo_grading(hunter_bot, target_bot, next_move_fcn, OTHER = None):
    """Returns True if your next_move_fcn successfully guides the hunter_bot
    to the target_bot. This function is here to help you understand how we 
    will grade your submission."""
    max_distance = 0.98 * target_bot.distance # 0.98 is an example. It will change.
    separation_tolerance = 0.02 * target_bot.distance # hunter must be within 0.02 step size to catch target
    caught = False
    ctr = 0

    # We will use your next_move_fcn until we catch the target or time expires.
    while not caught and ctr < 1000:

        # Check to see if the hunter has caught the target.
        hunter_position = (hunter_bot.x, hunter_bot.y)
        target_position = (target_bot.x, target_bot.y)
        separation = distance_between(hunter_position, target_position)
        if separation < separation_tolerance:
            print "You got it right! It took you ", ctr, " steps to catch the target."
            caught = True

        # The target broadcasts its noisy measurement
        target_measurement = target_bot.sense()

        # This is where YOUR function will be called.
        turning, distance, OTHER = next_move_fcn(hunter_position, hunter_bot.heading, target_measurement, max_distance, OTHER)
        
        # Don't try to move faster than allowed!
        if distance > max_distance:
            distance = max_distance

        # We move the hunter according to your instructions
        hunter_bot.move(turning, distance)

        # The target continues its (nearly) circular motion.
        target_bot.move_in_circle()

        ctr += 1            
        if ctr >= 1000:
            print "It took too many steps to catch the target."
    return caught



def angle_trunc(a):
    """This maps all angles to a domain of [-pi, pi]"""
    while a < 0.0:
        a += pi * 2
    return ((a + pi) % (pi * 2)) - pi

def get_heading(hunter_position, target_position):
    """Returns the angle, in radians, between the target and hunter positions"""
    hunter_x, hunter_y = hunter_position
    target_x, target_y = target_position
    heading = atan2(target_y - hunter_y, target_x - hunter_x)
    heading = angle_trunc(heading)
    return heading

def naive_next_move(hunter_position, hunter_heading, target_measurement, max_distance, OTHER):
    """This strategy always tries to steer the hunter directly towards where the target last
    said it was and then moves forwards at full speed. This strategy also keeps track of all 
    the target measurements, hunter positions, and hunter headings over time, but it doesn't 
    do anything with that information."""
    if not OTHER: # first time calling this function, set up my OTHER variables.
        measurements = [target_measurement]
        hunter_positions = [hunter_position]
        hunter_headings = [hunter_heading]
        OTHER = (measurements, hunter_positions, hunter_headings) # now I can keep track of history
    else: # not the first time, update my history
        OTHER[0].append(target_measurement)
        OTHER[1].append(hunter_position)
        OTHER[2].append(hunter_heading)
        measurements, hunter_positions, hunter_headings = OTHER # now I can always refer to these variables
    
    heading_to_target = get_heading(hunter_position, target_measurement)
    heading_difference = heading_to_target - hunter_heading
    turning =  heading_difference # turn towards the target
    distance = max_distance # full speed ahead!
    return turning, distance, OTHER

# target = robot(0.0, 10.0, 0.0, 2*pi / 30, 1.5)
# measurement_noise = .05*target.distance
# target.set_noise(0.0, 0.0, measurement_noise)

# hunter = robot(-10.0, -10.0, 0.0)

# print demo_grading(hunter, target, naive_next_move)





