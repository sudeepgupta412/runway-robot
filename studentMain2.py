# ----------
# Part Two
#
# Now we'll make the scenario a bit more realistic. Now Traxbot's
# sensor measurements are a bit noisy (though its motions are still
# completetly noise-free and it still moves in an almost-circle).
# You'll have to write a function that takes as input the next
# noisy (x, y) sensor measurement and outputs the best guess 
# for the robot's next position.
#
# ----------
# YOUR JOB
#
# Complete the function estimate_next_pos. You will be considered 
# correct if your estimate is within 0.01 stepsizes of Traxbot's next
# true position. 
#
# ----------
# GRADING
# 
# We will make repeated calls to your estimate_next_pos function. After
# each call, we will compare your estimated position to the robot's true
# position. As soon as you are within 0.01 stepsizes of the true position,
# you will be marked correct and we will tell you how many steps it took
# before your function successfully located the target bot.

# These import steps give you access to libraries which you may (or may
# not) want to use.
from robot import *  # Check the robot.py tab to see how this works.
from math import *
from matrix import * # Check the matrix.py tab to see how this works.
import random

# This is the function you have to write. Note that measurement is a 
# single (x, y) point. This function will have to be called multiple
# times before you have enough information to accurately predict the
# next position. The OTHER variable that your function returns will be 
# passed back to your function the next time it is called. You can use
# this to keep track of important information over time.

### References: This code has been taken from my previous submission to this course in Spring 2018

def filter(x, P, measurements, F, H, I, R):
    for n in range(len(measurements)):
        
        # measurement update
        z = matrix([[measurement[0]], [measurement[1]]])
        #y = Z.transpose() - (H * x)
        y = Z - (H * x)
        S = H * P * H.transpose() + R
        K = P * H.transpose() * S.inverse()
        x = x + (K * y)
        P = (I - (K * H)) * P
        
        # prediction
        x = (F * x) + u
        P = F * P * F.transpose()
        
        
    
    print 'x= '
    x.show()
    print 'P= '
    P.show()
    
    return x, P
def estimate_next_pos(measurement, OTHER = None):
    """Estimate the next (x, y) position of the wandering Traxbot
    based on noisy (x, y) measurements."""

    # You must return xy_estimate (x, y), and OTHER (even if it is None) 
    # in this order for grading purposes.
    #### DO NOT MODIFY ANYTHING ABOVE HERE ####
    #### fill this in, remember to use the matrix() function!: ####
    
    #P =  # initial uncertainty: 0 for positions x and y, 1000 for the two velocities
    #F =  # next state function: generalize the 2d version to 4d
    #H =  # measurement function: reflect the fact that we observe x and y but not the two velocities
    #R =  # measurement uncertainty: use 2x2 matrix with 0.1 as main diagonal
    #I =  # 4d identity matrix
    #xy_estimate = (3.2, 9.1)   
    

    if OTHER is None:
        OTHER = []
        heading = 0
        turn = 0
        distance1 = 0
        xy_estimate = (measurement[0], measurement[1])
        #x = matrix([[measurement[0]], [measurement[1]], [heading], [turn], [distance1]])
        x = matrix([[measurement[0]], [measurement[1]], [heading], [turn], [distance1]])
        P = matrix([[1000,0,0,0,0],[0,1000,0,0,0],[0,0,1000,0,0],[0,0,0,1000,0],[0,0,0,0,1000]])
        OTHER.append((x,P))
        return measurement, OTHER
    elif len(OTHER) == 1:
        P = matrix([[1000,0,0,0,0],[0,1000,0,0,0],[0,0,1000,0,0],[0,0,0,1000,0],[0,0,0,0,1000]])
        x = matrix([[measurement[0]], [measurement[1]], [0], [0], [0]])
        OTHER.append((x, P))
        return measurement, OTHER
    elif len(OTHER) == 2:
        P = matrix([[1000,0,0,0,0],[0,1000,0,0,0],[0,0,1000,0,0],[0,0,0,1000,0],[0,0,0,0,1000]])
        x = matrix([[measurement[0]], [measurement[1]], [0], [0], [0]])
        OTHER.append((x, P))
        x_dash = OTHER[-1][0]
        x_new = 2 * OTHER[-1][0].value[0][0] - OTHER[-2][0].value[0][0]
        y_new = 2 * OTHER[-1][0].value[1][0] - OTHER[-2][0].value[1][0]
        return (x_new, y_new), OTHER
    elif len(OTHER) == 3:
        P = matrix([[1000,0,0,0,0],[0,1000,0,0,0],[0,0,1000,0,0],[0,0,0,1000,0],[0,0,0,0,1000]])
        i = len(OTHER)
        current_measurement = measurement
        prev_measurement = (OTHER[-1][0].value[0][0],OTHER[-1][0].value[1][0])
        prev_measurement2 = (OTHER[-2][0].value[0][0],OTHER[-2][0].value[1][0])
        distance1 = distance_between(prev_measurement,current_measurement)
        heading = atan2(current_measurement[1] - prev_measurement[1], current_measurement[0] - prev_measurement[0])
        heading_old = atan2(prev_measurement[1] - prev_measurement2[1], prev_measurement[0] - prev_measurement2[0])
        turn = heading - heading_old
        est_heading = angle_trunc(turn + heading)
        est_distance = distance1
        est_x = current_measurement[0] + est_distance * cos(est_heading)
        est_y = current_measurement[1] + est_distance * sin(est_heading)
        #measurement_vector.append(measurement)
        angle = est_heading
        x = matrix([[est_x], [est_y], [est_heading], [turn], [est_distance]])
        OTHER.append((x, P))
        
        xy_estimate = (est_x, est_y)
        return (est_x, est_y), OTHER
    else:
#        if len(OTHER) == 4:
#            x = matrix([[OTHER[-1][0][0]], [OTHER[-1][0][1]], [OTHER[-1][0][2]], [OTHER[-1][0][3]], [OTHER[-1][0][4]]])

        i = len(OTHER)-1
        x = OTHER[i][0]
        P = OTHER[i][1]
        heading = x.value[2]
        turn = x.value[3]
        distance1 = x.value[4]
    
        H = matrix([[1,0,0,0,0],[0,1,0,0,0]])
        R = matrix([[measurement_noise,0],[0,measurement_noise]])
        I = matrix([[1,0,0,0,0],[0,1,0,0,0],[0,0,1,0,0],[0,0,0,1,0],[0,0,0,0,1]]) 
        u = matrix([[0.], [0.], [0.], [0.], [0.]])
        # measurement update
        Z = matrix([[measurement[0]],[measurement[1]]])
        y = Z - (H * x)
        S = H * P * H.transpose() + R
        K = P * H.transpose() * S.inverse()
        x = x + (K * y)
        #print x
        P = (I - (K * H)) * P
           
        # prediction
        distance1 = x.value[4][0]
        heading = x.value[2][0]
        turn = x.value[3][0]
        #x_dash = matrix([[x.value[0][0] + distance1 * cos(heading+turn)],[x.value[1][0] + distance1 * sin(heading+turn)],[heading+turn],[turn],[distance1]])
        x_dash = matrix([[x.value[0][0] + distance1 * cos(heading+turn)],[x.value[1][0] + distance1 * sin(heading+turn)],[heading+turn],[turn],[distance1]])
        
        dx_h = -distance1 * sin(heading + turn) 
        dx_t = -distance1 * sin(heading + turn)
        dx_d = cos(heading + turn)
        dy_h = distance1 * cos(heading + turn)
        dy_t = distance1 * cos(heading + turn)
        dy_d = sin(heading + turn) 
        F = matrix([[1,0,dx_h,dx_t,dx_d],[0,1,dy_h,dy_t,dy_d],[0,0,1,1,0],[0,0,0,1,0],[0,0,0,0,1]])
        
        P = F * P * F.transpose()
        OTHER.append((x_dash, P))
        xy_estimate = (x_dash.value[0][0],x_dash.value[1][0])
    
    #print("Estimate of Iteration " +str(len(OTHER)) + "estimated value = " + str(xy_estimate))
    return xy_estimate, OTHER

# A helper function you may find useful.
def distance_between(point1, point2):
    """Computes distance between point1 and point2. Points are (x, y) pairs."""
    x1, y1 = point1
    x2, y2 = point2
    return sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)

# This is here to give you a sense for how we will be running and grading
# your code. Note that the OTHER variable allows you to store any 
# information that you want. 
def demo_grading1(estimate_next_pos_fcn, target_bot, OTHER = None):
    localized = False
    distance_tolerance = 0.01 * target_bot.distance
    ctr = 0
    # if you haven't localized the target bot, make a guess about the next
    # position, then we move the bot and compare your guess to the true
    # next position. When you are close enough, we stop checking.
    while not localized and ctr <= 1000:
        ctr += 1
        measurement = target_bot.sense()
        position_guess, OTHER = estimate_next_pos_fcn(measurement, OTHER)
        target_bot.move_in_circle()
        true_position = (target_bot.x, target_bot.y)
        error = distance_between(position_guess, true_position)
        if error <= distance_tolerance:
            print "You got it right! It took you ", ctr, " steps to localize."
            localized = True
        if ctr == 1000:
            print "Sorry, it took you too many steps to localize the target."
    return localized

def demo_grading(estimate_next_pos_fcn, target_bot, OTHER = None):
    localized = False
    distance_tolerance = 0.01 * target_bot.distance
    ctr = 0
    # if you haven't localized the target bot, make a guess about the next
    # position, then we move the bot and compare your guess to the true
    # next position. When you are close enough, we stop checking.
    #For Visualization
    import turtle    #You need to run this locally to use the turtle module
    window = turtle.Screen()
    window.bgcolor('white')
    size_multiplier= 25.0  #change Size of animation
    broken_robot = turtle.Turtle()
    broken_robot.shape('turtle')
    broken_robot.color('green')
    broken_robot.resizemode('user')
    broken_robot.shapesize(0.1, 0.1, 0.1)
    measured_broken_robot = turtle.Turtle()
    measured_broken_robot.shape('circle')
    measured_broken_robot.color('red')
    measured_broken_robot.resizemode('user')
    measured_broken_robot.shapesize(0.1, 0.1, 0.1)
    prediction = turtle.Turtle()
    prediction.shape('arrow')
    prediction.color('blue')
    prediction.resizemode('user')
    prediction.shapesize(0.1, 0.1, 0.1)
    prediction.penup()
    broken_robot.penup()
    measured_broken_robot.penup()
    #End of Visualization
    while not localized and ctr <= 10:
        ctr += 1
        measurement = target_bot.sense()
        position_guess, OTHER = estimate_next_pos_fcn(measurement, OTHER)
        target_bot.move_in_circle()
        true_position = (target_bot.x, target_bot.y)
        error = distance_between(position_guess, true_position)
        if error <= distance_tolerance:
            print "You got it right! It took you ", ctr, " steps to localize."
            localized = True
        if ctr == 10:
            print "Sorry, it took you too many steps to localize the target."
        #More Visualization
        measured_broken_robot.setheading(target_bot.heading*180/pi)
        measured_broken_robot.goto(measurement[0]*size_multiplier, measurement[1]*size_multiplier-200)
        measured_broken_robot.stamp()
        broken_robot.setheading(target_bot.heading*180/pi)
        broken_robot.goto(target_bot.x*size_multiplier, target_bot.y*size_multiplier-200)
        broken_robot.stamp()
        prediction.setheading(target_bot.heading*180/pi)
        prediction.goto(position_guess[0]*size_multiplier, position_guess[1]*size_multiplier-200)
        prediction.stamp()
        #End of Visualization
    return localized

# This is a demo for what a strategy could look like. This one isn't very good.
def naive_next_pos(measurement, OTHER = None):
    """This strategy records the first reported position of the target and
    assumes that eventually the target bot will eventually return to that 
    position, so it always guesses that the first position will be the next."""
    if not OTHER: # this is the first measurement
        OTHER = measurement
    xy_estimate = OTHER 
    return xy_estimate, OTHER

# This is how we create a target bot. Check the robot.py file to understand
# How the robot class behaves.
test_target = robot(2.1, 4.3, 0.5, 2*pi / 34.0, 1.5)
measurement_noise = 0.05 * test_target.distance
test_target.set_noise(0.0, 0.0, measurement_noise)

demo_grading(naive_next_pos, test_target)




